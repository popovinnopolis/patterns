package Structural.Adapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by evgenijpopov on 04.07.17.
 */
public class FbSocialWeb {
    String getFriend(long id) {
        return "friend " + id + " in FB!";
    }

    List<String> getWall(long id, boolean showHidden) {
        if (id > 1_000_000 && !showHidden) {
            return new ArrayList<>();
        } else {
            List<String> aaa = new ArrayList<>();
            aaa.add("trololo");
            aaa.add("ululu");
            return aaa;
        }
    }

    public void pay(float amount, float valuteCoeff, long idReceiver) {
        System.out.println("You pay " + amount * valuteCoeff + " to " + idReceiver + " in FB!");
    }

}
