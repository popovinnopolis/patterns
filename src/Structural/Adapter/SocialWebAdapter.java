package Structural.Adapter;

import java.util.List;

/**
 * Created by evgenijpopov on 04.07.17.
 */
public class SocialWebAdapter implements ISocialWeb {
    private Object iSocialWeb;

    public SocialWebAdapter(Object iSocialWeb) {
        this.iSocialWeb = iSocialWeb;
    }

    @Override
    public String getFriend(long id) {
        if (iSocialWeb instanceof VcSocialWeb) {
            return ((VcSocialWeb) iSocialWeb).getFriend((int) id, true);
        } else if ((iSocialWeb instanceof FbSocialWeb)) {
            return ((FbSocialWeb) iSocialWeb).getFriend(id);
        }
        return null;
    }

    @Override
    public List<String> getWall(long id, boolean showHidden) {
        if (iSocialWeb instanceof VcSocialWeb) {
            return ((VcSocialWeb) iSocialWeb).getWall((long) id, 100, showHidden);
        } else if ((iSocialWeb instanceof FbSocialWeb)) {
            return ((FbSocialWeb) iSocialWeb).getWall(id, showHidden);
        }
        return null;
    }

    @Override
    public void pay(float amount, int valuteCoeff, long idReceiver) {

    }
}
