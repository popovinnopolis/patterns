package Structural.Adapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by evgenijpopov on 04.07.17.
 */
public class VcSocialWeb {
    String getFriend(long id, boolean showHidden) {
        if (id < 1000 && !showHidden) {
            return "";
        } else {
            return "friend " + id;
        }
    }

    List<String> getWall(long id, int likes, boolean showHidden)
    {
        if (id>likes) {
            return new ArrayList<>();
        } else {
            List<String> aaa = new ArrayList<>();
            aaa.add("ololo");
            aaa.add("azaza");
            return aaa;
        }
    }

    boolean pay(float amount, float valuteCoeff, long idReceiver){
        System.out.println("You pay "+amount+" to "+ idReceiver + " in vk!");
        return true;
    }

}
