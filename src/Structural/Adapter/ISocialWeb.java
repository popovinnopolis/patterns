package Structural.Adapter;

import java.util.List;

/**
 * Created by evgenijpopov on 04.07.17.
 */
public interface ISocialWeb {
    String getFriend(long id);
    List<String> getWall(long id, boolean showHidden);
    void pay(float amount, int valuteCoeff, long idReceiver);
}
