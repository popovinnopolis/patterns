package Structural.Decorator;

/**
 * Created by evgenijpopov on 04.07.17.
 */
public class Main {

    public static void main(String[] args) {
        Loan loan = new Loan();

        LoanInsurance loanInsurance = new LoanInsurance(loan);
        LoanCandy loanCandy = new LoanCandy(loanInsurance);
        LoanRefinancing loanRefinancing = new LoanRefinancing(loanCandy);

        loanRefinancing.addConditions();

    }
}
