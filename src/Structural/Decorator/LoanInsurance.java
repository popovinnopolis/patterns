package Structural.Decorator;

/**
 * Created by evgenijpopov on 04.07.17.
 */
public class LoanInsurance extends LoanComponent {
    public LoanInsurance(Component component) {
        super(component);
    }

    @Override
    public void addConditions() {
        if (component != null) {
            component.addConditions();
        }
        System.out.println("LoanInsurance's conditions");
    }

}
