package Structural.Decorator;

/**
 * Created by evgenijpopov on 04.07.17.
 */
public class LoanRefinancing extends LoanComponent {
    public LoanRefinancing(Component component) {
        super(component);
    }

    @Override
    public void addConditions() {
        if (component != null) {
            component.addConditions();
        }
        System.out.println("LoanRefinancing's conditions");
    }

}
