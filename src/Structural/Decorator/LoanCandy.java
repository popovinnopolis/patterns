package Structural.Decorator;

/**
 * Created by evgenijpopov on 04.07.17.
 */
public class LoanCandy extends LoanComponent {
    public LoanCandy(Component component) {
        super(component);
    }

    @Override
    public void addConditions() {
        if (component != null) {
            component.addConditions();
        }
        System.out.println("LoanCandy's conditions");
    }

}
