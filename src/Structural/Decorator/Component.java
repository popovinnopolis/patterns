package Structural.Decorator;

/**
 * Created by evgenijpopov on 04.07.17.
 */
public interface Component {
    public void addConditions();
}
