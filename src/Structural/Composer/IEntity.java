package Structural.Composer;

/**
 * Created by evgenijpopov on 04.07.17.
 */
public interface IEntity {
    Number value();

    void add(IEntity entity);

    void sub(IEntity entity);

    IEntity getSubEntity(int index);
}
