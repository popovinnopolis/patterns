package Structural.Composer;

/**
 * Created by evgenijpopov on 04.07.17.
 */
public class Main {
    public static void main(String[] args) {
        IEntity clobalVault = new ListEntities();

        IEntity house1 = new LongValue(72L);
        IEntity house2 = new LongValue(36L);
        IEntity house3 = new LongValue(144L);
        IEntity yard1 = new ListEntities(house1, house2);
        IEntity yard2 = new ListEntities(house3);
        IEntity district1 = new ListEntities(yard1);
        IEntity district2 = new ListEntities(yard2);
        IEntity city1 = new ListEntities(district1, district2);
        IEntity area1 = new ListEntities(city1);

        System.out.println(area1.value());

    }
}
