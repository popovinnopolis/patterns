package Structural.Composer;

/**
 * Created by evgenijpopov on 04.07.17.
 */
public class LongValue implements IEntity {
    private Long value;

    public LongValue(Long value) {
        this.value = value;
    }

    @Override
    public Number value() {
        return value;
    }

    @Override
    public void add(IEntity entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void sub(IEntity entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public IEntity getSubEntity(int index) {
        throw new UnsupportedOperationException();
    }
}
