package Structural.Composer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by evgenijpopov on 04.07.17.
 */
public class ListEntities implements IEntity {
    private List<IEntity> entities;
    private long counter;

    public ListEntities(IEntity... entities) {
        this.entities = new ArrayList<>();
        for (IEntity en : entities) {
            this.entities.add(en);
        }
    }

    @Override
    public Number value() {
        long result = 0L;
        for (IEntity en : entities) {
            result += en.value().longValue();
        }
        return null;
    }

    @Override
    public void add(IEntity entity) {
        entities.add(entity);
    }

    @Override
    public void sub(IEntity entity) {
        if (entity instanceof LongValue) {
            entities.add(new LongValue(-1 * entity.value().longValue()));
        }
    }

    @Override
    public IEntity getSubEntity(int index) {
        return entities.get(index);
    }
}
