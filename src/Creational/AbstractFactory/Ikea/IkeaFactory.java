package Creational.AbstractFactory.Ikea;

import Creational.AbstractFactory.*;

/**
 * Created by evgenijpopov on 03.07.17.
 */
public class IkeaFactory extends FurnitureFactory {
    @Override
    public Chair createChair() {
        return new IkeaChair();
    }

    @Override
    public Table createTable() {
        return new IkeaTable();
    }

    @Override
    public ArmChair createArmChair() {
        return new IkeaArmChair();
    }

    @Override
    public Sofa createSofa() {
        return new IkeaSofa();
    }
}
