package Creational.FactoryMethod;


/**
 * Created by evgenijpopov on 03.07.17.
 */
public class Main {
    public static void main(String[] args) {
        House house = House.createHouse(HouseType.WoodenHouse);
    }
}
