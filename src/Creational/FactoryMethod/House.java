package Creational.FactoryMethod;

/**
 * Created by evgenijpopov on 03.07.17.
 */
public abstract class House {
    public static House createHouse(HouseType house) {
        if (house == HouseType.StoneHouse) {
            return new StoneHouse();
        } else if (house == HouseType.WoodenHouse) {
            return new WoodenHouse();
        }
        return null;
    }

}
