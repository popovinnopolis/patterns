package Creational.FactoryMethod;

/**
 * Created by evgenijpopov on 03.07.17.
 */
public enum HouseType {
    WoodenHouse,
    StoneHouse
}
