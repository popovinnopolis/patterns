package Creational.Prototype;

/**
 * Created by evgenijpopov on 03.07.17.
 */
public interface Copyable {
    Copyable copy();
}
