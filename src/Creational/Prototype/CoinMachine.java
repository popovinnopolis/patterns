package Creational.Prototype;

/**
 * Created by evgenijpopov on 04.07.17.
 */
public class CoinMachine {
    private Coin coin;

    public CoinMachine(Coin coin) {
        this.coin = coin;
    }

    public Coin makeCoin(int value, Coin.Type type, boolean rare) throws CloneNotSupportedException {
        Coin cn = this.coin.clone();
        cn.setType(type);
        cn.setValue(value);
        cn.setRare(rare);
        return cn;
    }
}
