package Creational.Prototype;

/**
 * Created by evgenijpopov on 03.07.17.
 */
public class Coin implements Cloneable {
    protected Type type;
    protected boolean rare;
    protected int value;

    protected enum Type {
        Rouble, Dollar, Pound
    }

    public Coin(int value) {
        this.value = value;
    }

    public Coin() {
        value = 0;
    }

    @Override
    protected Coin clone() throws CloneNotSupportedException {
        return (Coin) super.clone();
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setRare(boolean rare) {
        this.rare = rare;
    }
}
