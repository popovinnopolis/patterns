package Creational.Prototype;

/**
 * Created by evgenijpopov on 03.07.17.
 */
public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {

        Coin prot = new Coin();
        CoinMachine cm = new CoinMachine(prot);
        Coin dollarCoin = cm.makeCoin(10, Coin.Type.Dollar, true);
        Coin roubleCoin = cm.makeCoin(10, Coin.Type.Rouble, false);
        Coin poundCoin = cm.makeCoin(10, Coin.Type.Pound, true);
        System.out.println(dollarCoin);
    }

}
