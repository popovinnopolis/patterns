package Behavioural.Observer;


/**
 * Created by evgenijpopov on 05.07.17.
 */
public interface Observable {
    void addObserver(Observer observer);
    void removeObserver(Observer observer);
    void notifyAllObserver();
}
