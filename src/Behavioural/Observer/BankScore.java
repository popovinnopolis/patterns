package Behavioural.Observer;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by evgenijpopov on 05.07.17.
 */
public class BankScore implements Observable {
    List<Observer> observers = new ArrayList<>();
    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }


    @Override
    public void notifyAllObserver() {
        for (Observer ob: observers) {
            ob.message("Msg");
        }

    }
}
