package Behavioural.Observer;

/**
 * Created by evgenijpopov on 05.07.17.
 */
public class IRS  implements Observer{
    @Override
    public void message(Object ob) {
        System.out.println(ob.toString());
    }
}
