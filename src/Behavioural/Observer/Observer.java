package Behavioural.Observer;

/**
 * Created by evgenijpopov on 05.07.17.
 */
public interface Observer {
    void message(Object ob);
}
