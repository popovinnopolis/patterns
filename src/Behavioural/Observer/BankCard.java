package Behavioural.Observer;

import java.util.*;

/**
 * Created by evgenijpopov on 05.07.17.
 */
public class BankCard implements Observer {

    @Override
    public void message(Object ob) {
        System.out.println(ob.toString());
    }
}
