package Behavioural.Observer;

/**
 * Created by evgenijpopov on 05.07.17.
 */
public class Main {
    public static void main(String[] args) {
        BankCard bankCard1 = new BankCard();
        BankCard bankCard2 = new BankCard();
        IRS irs = new IRS();

        BankScore bankScore = new BankScore();
        bankScore.addObserver(bankCard1);
        bankScore.addObserver(bankCard2);
        bankScore.addObserver(irs);
        bankScore.notifyAllObserver();

        bankScore.removeObserver(bankCard1);
        bankScore.notifyAllObserver();

    }
}
