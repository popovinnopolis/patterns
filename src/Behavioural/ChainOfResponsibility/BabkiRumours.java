package Behavioural.ChainOfResponsibility;

/**
 * Created by evgenijpopov on 05.07.17.
 */
public class BabkiRumours extends Rumour {
    boolean isAlive;

    public BabkiRumours(boolean isAlive) {
        this.isAlive = isAlive;
    }

    @Override
    public void obs(String s) {
        System.out.println("Babka said: " + s);
        if (!isAlive) {
            System.out.println("Sorry, but babka is dead!");
            return;
        }
        if (next != null) {
            next.obs(s);
        }
    }
}
