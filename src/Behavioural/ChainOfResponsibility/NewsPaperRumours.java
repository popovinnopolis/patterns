package Behavioural.ChainOfResponsibility;

/**
 * Created by evgenijpopov on 05.07.17.
 */
public class NewsPaperRumours extends Rumour {
    int balance;

    public NewsPaperRumours(int balance) {
        this.balance = balance;
    }

    @Override
    public void obs(String s) {

        if (balance <=0) {
            System.out.println("Sorry");
            return;
        }

        if (next != null){
            next.obs(s);
        }
    }
}
