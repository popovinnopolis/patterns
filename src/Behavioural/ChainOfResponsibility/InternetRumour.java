package Behavioural.ChainOfResponsibility;

/**
 * Created by evgenijpopov on 05.07.17.
 */
public class InternetRumour extends Rumour {
    boolean isConnected;

    public InternetRumour(boolean isConnected) {
        this.isConnected = isConnected;
    }

    @Override
    public void obs(String s) {
        if (!isConnected) {
            System.out.println("End of life!");
            return;
        }
        System.out.println("In Inets said: " + s);
        if (next != null) {
            next.obs(s);
        }
    }
}
