package Behavioural.ChainOfResponsibility;

/**
 * Created by evgenijpopov on 05.07.17.
 */
abstract public class Rumour implements Rumours {
    protected Rumour next;

    public void setNext(Rumour next) {
        this.next = next;
    }
}
