package Behavioural.ChainOfResponsibility;

/**
 * Created by evgenijpopov on 05.07.17.
 */
public class Main {
    public static void main(String[] args) {

        BabkiRumours babka = new BabkiRumours(true);
        NewsPaperRumours newsPaperRumours = new NewsPaperRumours(1000);
        InternetRumour internetRumour = new InternetRumour(true);

        babka.setNext(newsPaperRumours);
        newsPaperRumours.setNext(internetRumour);
        babka.obs("Gref is arrive");
    }

}
