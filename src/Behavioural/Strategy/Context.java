package Behavioural.Strategy;

import java.math.BigDecimal;

/**
 * Created by evgenijpopov on 05.07.17.
 */
public class Context {
    PaymentStrategy paymentStrategy;

    long cardBalance = 0;
    long income;

    public Context(long cardBalance, long income) {
        this.cardBalance = cardBalance;
        this.income = income;
        calculateStrategy();
    }

    public void pay(long amount) {
        paymentStrategy.pay(amount);
    }

    private void calculateStrategy() {
        if (cardBalance < 0) {
            paymentStrategy = new EconomyStrategy();
        }
        if (cardBalance >= 0 && income < 1000) {
            paymentStrategy = new SimpleStrategy();
        }
        if (cardBalance >= 0 && income > 1000) {
            paymentStrategy = new FastStrategy();
        }
    }
}
