package Behavioural.Strategy;

/**
 * Created by evgenijpopov on 05.07.17.
 */
public interface PaymentStrategy {
    void pay(long amount);
}
