package Behavioural.Strategy;

/**
 * Created by evgenijpopov on 05.07.17.
 */
public class EconomyStrategy implements PaymentStrategy {
    @Override
    public void pay(long amount) {
        System.out.println("I pay " + amount / 4);
        System.out.println("I pay " + amount / 4);
        System.out.println("I pay " + amount / 4);
        System.out.println("I pay " + amount / 4);
    }
}
