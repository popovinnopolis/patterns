package Behavioural.Strategy;

/**
 * Created by evgenijpopov on 05.07.17.
 */
public class Main {
    public static void main(String[] args) {
        Context context = new Context(50, 50);
        context.pay(40);
    }
}
