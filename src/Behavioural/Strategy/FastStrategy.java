package Behavioural.Strategy;

/**
 * Created by evgenijpopov on 05.07.17.
 */
public class FastStrategy implements PaymentStrategy {
    @Override
    public void pay(long amount) {
        System.out.println("I pay " + amount);
    }
}
